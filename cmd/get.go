/*
Copyright © 2020 Vaibhav Kaushik <opensource@vaibhavk.in>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/spf13/cobra"
	apiclient "gitlab.com/vaibhav_kaushik/kv-store-rest/client"
	"gitlab.com/vaibhav_kaushik/kv-store-rest/client/operations"
)

var key string

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "get value of the given key",
	Long: `Similar to 'curl -x GET' you need to pass the key
	and you will get the value associated with it.For example:

	$ client get --key foo
	5`,
	Run: func(cmd *cobra.Command, args []string) {
		client := apiclient.New(httptransport.New("localhost:9200", "", []string{"http"}), strfmt.Default)
		resp, err := client.Operations.GetValue(&operations.GetValueParams{Key: key})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%#v\n", resp.Payload)
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	getCmd.Flags().StringVar(&key, "key", "", "Provide a key to get the value")
	getCmd.MarkFlagRequired("key")
}
