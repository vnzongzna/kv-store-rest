// Code generated by go-swagger; DO NOT EDIT.

/*
Package restapi key value store
Project for Grofers internship candidate, a key value store


    Schemes:
      http
    Host: localhost
    BasePath: /
    Version: 1.0.0

    Consumes:
    - application/json

    Produces:
    - application/json

swagger:meta
*/
package restapi
