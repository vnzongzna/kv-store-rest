// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"
	"sync"

	errors "github.com/go-openapi/errors"
	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"

	"gitlab.com/vaibhav_kaushik/kv-store-rest/models"
	"gitlab.com/vaibhav_kaushik/kv-store-rest/restapi/operations"
)

//go:generate swagger generate server --target ../../kv-store-rest --name Kv --spec ../spec/swagger.yml

// most basic data structure to store the key-value pair
var pairs = make(map[string]string)

// handling concurrency with locking
var itemsLock = &sync.Mutex{}

func configureFlags(api *operations.KvAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ }
}

func addKeyValuePair(pair *models.Pair) error {
	if pair == nil {
		return errors.New(500, "pair must be present")
	}
	itemsLock.Lock()
	defer itemsLock.Unlock()

	pairs[pair.Key] = swag.StringValue(pair.Value)
	return nil
}

func deletePair(key string) error {
	itemsLock.Lock()
	defer itemsLock.Unlock()

	_, exists := pairs[key]
	if !exists {
		return errors.NotFound("not found: key %s", key)
	}

	delete(pairs, key)
	return nil
}
func configureAPI(api *operations.KvAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.PostHandler = operations.PostHandlerFunc(func(params operations.PostParams) middleware.Responder {
		if err := addKeyValuePair(params.Body); err != nil {
			return operations.NewPostObjectDefault(500).WithPayload(&models.Error{Code: 500, Message: swag.String(err.Error())})
		}
		return operations.NewPostObjectCreated().WithPayload(params.Body)
	})

	api.DestroyOneHandler = operations.DestroyOneHandlerFunc(func(params operations.DestroyOneParams) middleware.Responder {
		if err := deletePair(params.Key); err != nil {
			return operations.NewDestroyOneDefault(500).WithPayload(&models.Error{Code: 500, Message: swag.String(err.Error())})
		}
		return operations.NewDestroyOneNoContent()
		// return middleware.NotImplemented("operation .DestroyOne has not yet been implemented")
	})

	api.GetValueHandler = operations.GetValueHandlerFunc(func(params operations.GetValueParams) middleware.Responder {
		if val, ok := pairs[params.Key]; ok {
			return operations.NewGetValueOK().WithPayload(&models.Success{Value: swag.String(val)})
		} else {
			return operations.NewGetValueDefault(500).WithPayload(&models.Error{Code: 500, Message: swag.String("key doesn't exist")})
		}
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
